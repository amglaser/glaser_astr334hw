import numpy as np
import math as ma
import matplotlib.pyplot as plt

#np.set_printoptions(precision=20)

#ncounts = open('astr344_homework_materials/model_smg.dat') #I keep my copy of the data in a separate sub-folder
ncounts = open('model_smg.dat')
ndata = ncounts.read()


data_list = ndata.split("\n")
data_list = data_list[:-1] #Removes extra list index from split 

for i in range(0, len(data_list)):
    data_list[i] = data_list[i].split("\t")
    #List containing coordinate pairs in a sub-list
    #(Luminosity, Number of Galaxies above this luminosity)
    data_list[i][0] = np.float64(data_list[i][0])
    data_list[i][1] = np.float64(data_list[i][1])
    

#logcounts = open('astr344_homework_materials/ncounts_850.dat') #I keep my copy of the data in a separate sub-folder
logcounts = open('ncounts_850.dat')
logdata = logcounts.read()


log_list = logdata.split("\n")
log_list = log_list[:-1] #Removes extra list index from split 

for i in range(0, len(log_list)):
    log_list[i] = log_list[i].split()
    #List containing coordinate pairs in a sub-list
    #(Log of Luminosity, Log of dN/dL)
    log_list[i][0] = np.float64(log_list[i][0])
    log_list[i][1] = np.float64(log_list[i][1])

slope = []

for i in range(0, len(data_list)):
    if (i!= 0) and (i!= len(data_list)-1):
        h1 = data_list[i][0] - data_list[i-1][0]
        h2 = data_list[i+1][0] - data_list[i][0]
        
        temp_slope = ((h1/(h2*(h1+h2)))*data_list[i+1][1]) - (((h1-h2)/(h2*h1))*data_list[i][1]) - ((h2/(h1*(h1+h2)))*data_list[i-1][1])
        slope += [[data_list[i][0], temp_slope]]
        
for i in range(0,len(slope)):
    slope[i][0] = ma.log(abs(slope[i][0]), 10)
    slope[i][1] = ma.log(abs(slope[i][1]), 10)

x_mine = []
y_mine = []

for i in range(0,len(slope)):
    x_mine += [slope[i][0]]
    y_mine += [slope[i][1]]

x_log = []
y_log = []

for i in range(0,len(log_list)):
    x_log += [log_list[i][0]]
    y_log += [log_list[i][1]]
    

plt.scatter(x_log, y_log, c= 'b')
plt.scatter(x_mine, y_mine, c= 'r')
plt.show()
