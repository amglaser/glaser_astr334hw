import numpy as np

sing_sum = np.float32(0)
doub_sum = np.float64(0)

for i in range(0,6):
    sing_sum += np.float32(1.0/(3**i))
    doub_sum += np.float64(1.0/(3**i))

ab_err = doub_sum - sing_sum
rel_err = ab_err/doub_sum

print "For n=5... \n Absolute error:", ab_err, "\n Relative error:", rel_err

### For n =20...

sing_sum = np.float32(0)
doub_sum = np.float64(0)

for i in range(0,21):
    sing_sum += np.float32(1.0/(3**i))
    doub_sum += np.float64(1.0/(3**i))

ab_err = doub_sum - sing_sum
rel_err = ab_err/doub_sum

print "\n For n=20... \n Absolute error:", ab_err, "\n Relative error:", rel_err