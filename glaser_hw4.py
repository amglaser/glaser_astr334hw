import numpy as np
import math as math
import matplotlib.pyplot as plt

def r(z):
    z = np.float64(z)
    C= np.float64(3.0*(10.0**3))
    OM = np.float64(0.3)
    OA = np.float64(0.7)
    return (C/(math.sqrt((OM*((1.0 + z)**3))+OA)))


def trap_integrate(n):
    points_x= []
    points_y = []
    N = np.float64(n)
    
    for z2 in range(1,6):
        temp_intgl = 0
        for i in range(0,n*z2): #Integration is partitioned into pieces of length 1/N
            temp_intgl += ((r((i+1.0)/N) + r(i/N))/(2.0*N))                       
        temp_intgl = temp_intgl/np.float64((1 + z2))
        points_x += [z2]
        points_y += [temp_intgl]
    
    #We compare this to the np.trapz built in python command...
    nump_x = [] 
    nump_y = []
    y_vals =[]
    
    for i in range(0, n*5 +1):
        y_vals += [r(i/N)]
    
    for z2 in range(1,6):
        nump_x += [z2]
        nump_y += [np.trapz(y_vals[:z2*n], x=None, dx=(1.0/N))/np.float64((1 + z2))]
    
    print points_x
    print points_y
    print nump_y

    plt.scatter(nump_x, nump_y, c='b') #np.trapz plot in blue
    plt.scatter(points_x, points_y, c='r') #My approximation of the function in red
    plt.ylim([0,1300])
    plt.show()


#trap_integrate(20)
#trap_integrate(200) #Difference from N=20 is <1. However np.trapz data is more closely aligned.
trap_integrate(2000) #Difference from N=20 is <1. Also np.trapz data differs even less than before.
#trap_integrate(20000) #Difference from N=20 is <1. Same true for np.trapz data.
