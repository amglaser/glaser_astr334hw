import numpy as np
import random
import math as ma
from datetime import datetime
from datetime import timedelta



###########Part 1###################

def NR_root(f, df, a, b, n):
    
    a_org =a
    b_org = b
    
    x = random.uniform(a,b) #starts with a random float in range [a,b]
    count = 0
    
    while count < n*(10**10):
                
        if abs(f(x)) < 10**(-5):
            return x
        
        x = x - (f(x)/df(x))
        count += 1
    
    return NR_root(f,df, a_org,b_org) #in case count is maxed out, we restart the function with a new random initial choice for the first bisection



def f(x):
    X = np.float64(x)
    return np.sin(X)

def df(x):
    X = np.float64(x)
    return np.cos(X)

def g(x):
    X = np.float64(x)
    return (X**3)-X-2

def dg(x):
    X = np.float64(x)
    return 3*(X**2)-1

def y(x):
    X = np.float64(x)
    return -6 + X + (X**2)

def dy(x):
    X = np.float64(x)
    return 1 + 2*X


def Bisec(f, a, b, n): #find a root of function f() on interval [a,b] #Increase max iteration count by factor of n #By default set n=1
    
    a_org =a
    b_org = b
    
    mid = random.uniform(a,b) #starts with a random float in range [a,b]
    count = 0
    
    while count < n*(10**10):
                
        if abs(f(mid)) < 10**(-5):
            return mid
        if (f(a)/f(mid)) < 0:
            b = mid
        if (f(b)/f(mid)) < 0:
            a = mid
        
        mid = (a+b)/2
        count += 1
    
    return Bisec(f,a_org,b_org) #in case count is maxed out, we restart the function with a new random initial choice for the first bisection



###########Part 2###################

#We subtract the intensity and consider it as part of the function to
#turn the Temp of interest into a root. Then we can find T using the NR-method

def B(x):
    X = np.float64(x) #temperature
    c = np.float64(2.99792458*(10.0**10))
    h = np.float64(6.6260755*(10.0**(-27)))
    v = np.float64((c/870.0)*(10.0**4))
    k = np.float64(1.380658*(10.0**(-16)))
    
    return ((2.0*h*(v**3))/(c**2))*(1.0/(ma.exp((h*v)/(k*X)) - 1.0)) - (1.25*(10.0**(-12)))


def dB(x):
    X = np.float64(x) #temperature
    c = np.float64(2.99792458*(10.0**10))
    h = np.float64(6.6260755*(10.0**(-27)))
    v = np.float64((c/870.0)*(10.0**4))
    k = np.float64(1.380658*(10.0**(-16)))
    
    return ((2.0*h*(v**3))/(c**2))*(1.0/(ma.exp((h*v)/(k*X)) - 1.0))*((h*v)/(k*(X**2)))*((ma.exp((h*v)/(k*X)))/(ma.exp((h*v)/(k*X)) - 1))


def hp_NR_root(f, df, a, b, n): #Since B has such a small output we need a more precise root-finder
    
    a_org =a
    b_org = b
    
    x = random.uniform(a,b) #starts with a random float in range [a,b]
    count = 0
    
    while count < n*(10**10):                
        if abs(f(x)) < 10**(-25):
            return x
        
        x = x - (f(x)/df(x))
        count += 1
    
    return hp_NR_root(f, df, a_org,b_org) #in case count is maxed out, we restart the function with a new random initial choice for the first bisection


###########Part 1###################

print"Part 1"

print "\n *---f root---*"
t1 = datetime.now()
print "Bisection method:", Bisec(f, 2, 4, 1) 
t2 = datetime.now()
print "time:", t2 - t1
print "-----"
t1 = datetime.now()
print "Newton-Rapshon method:", NR_root(f, df, 2, 4, 1) 
t2 = datetime.now()
print "time:", t2 - t1


print "\n *---g root---*"
t1 = datetime.now()
print "Bisection method:", Bisec(g, 1, 2, 1)
t2 = datetime.now()
print "time:", t2 - t1
print "-----"
t1 = datetime.now()
print "Newton-Rapshon method:", NR_root(g, dg, 1, 2, 1)
t2 = datetime.now()
print "time:", t2 - t1

print "\n *---y root---*"
t1 = datetime.now()
print "Bisection method:", Bisec(y, 0, 5, 1)
t2 = datetime.now()
print "time:", t2 - t1
print "-----"
t1 = datetime.now()
print "Newton-Rapshon method:", NR_root(y, dy, 0, 5, 1)
t2 = datetime.now()
print "time:", t2 - t1

###########Part 2###################

print "-\n-\n-\n-\nPart 2"

print"Temp:", hp_NR_root(B, dB, 25, 75, 1)


