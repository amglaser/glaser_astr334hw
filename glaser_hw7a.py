import random
import numpy as np

seed = 19.0

def my_rand(x,y):
    a = 5
    c = 1
    m = 2.0**5
    global seed
    
    seed = np.mod(seed*a + c, m)
    
    x = np.float64(x)
    y = np.float64(y)
    
    return np.float64((seed/m)*(y-x) + x)
    
    


def hit_miss_pi(n, k): #To find the value of pi, we calculate the area of the unit circle with radius inside the unit square (which has area 4)
    
    hit = 0
        
    for i in range(n):
        
        if k == 1:
            x = random.uniform(-1,1)
            y = random.uniform(-1,1)
        
        if k == 0:
            x = my_rand(-1,1)
            y = my_rand(-1,1)
        #print x,y #REMOVE LATER
    
        if x**2 + y**2 <= 1:
            hit += 1
    
    return 4.0*np.float64(hit)/(np.float64(n))


print "Built in random function..."
print "n = 10:", hit_miss_pi(10**1, 1)
print "n = 10**:3", hit_miss_pi(10**3, 1)
print "n = 10**5:", hit_miss_pi(10**5, 1)
print "n = 10**7:", hit_miss_pi(10**7, 1)#This takes extremely long and still isn't consistently 3.141
print "Note that not even n = 10**9 consistently hits 3.141"

print "\n\nMy random function..."
print "n = 10:", hit_miss_pi(10**1, 0)
print "n = 10**:3", hit_miss_pi(10**3, 0)
print "n = 10**5:", hit_miss_pi(10**5, 0)
print "n = 10**7:", hit_miss_pi(10**7, 0)#This takes extremely long and still isn't consistently 3.141