import numpy as np
import matplotlib.pyplot as plt

kcounts = open('astr344_homework_materials/ks_observables.dat') #I keep my copy of the data in a separate sub-folder
#kcounts = open('ks_observables.dat')
kdata = kcounts.read()

klist = kdata.split()
klist = klist[4:]

wco= [] #CO brightness
sfr = [] #star formation rate


for i in range(0, len(klist), 4):
    wco += [np.log10(np.float64(klist[i]))]
    sfr += [np.log10(np.float64(klist[i+1]))]
    

N = len(wco)

wco_org =wco
sfr_org = sfr

wco = np.array(wco)
sfr = np.array(sfr)

x = np.dot(wco, np.ones(N)) #sum of x's
x2 = np.dot(wco, wco) #sum of x^2's

y = np.dot(sfr, np.ones(N)) #sum of y's
y2 = np.dot(sfr, sfr) #sum of y^2's

xy = np.dot(wco, sfr) #sum of xy's

a1 = ((y*x2) - (x*xy))/((N*x2)-(x**2))
a2 = ((N*xy)-(y*x))/((N*x2)-(x**2))

Chi2 = 0

for i in range(N):
    Chi2 += (a1 + a2*wco[i] - sfr[i])**2

print "Xi^2:", Chi2

plt.scatter(wco_org, sfr_org)
plt.plot(wco_org, wco_org*a2 + a1)
plt.show()
