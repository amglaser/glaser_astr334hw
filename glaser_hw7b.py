import random
import numpy as np

def birthday_match(n):
    
    print "("+ str(n) + " trials) P > 0.5  at:",
    
    match_count = np.zeros(100) #We will assume that the 0.5 prob is bounded above by 100
    #                           #Each index m corresponds to the the sample with m+1 birthdays
    
    for i in range(n):
        
        bday_set = []
        x = random.randint(1,365)
        count = 0
        
        while (x not in bday_set) and (count < 100):
            bday_set += [x]
            x = random.randint(1,365)
            count += 1
        
        if count < 100:
            for j in range(count, 100):
                match_count[j] += 1.0 #In this sample every group of m >= count +1 is considered a successful trial
    
    N = np.float64(n)
    match_count *= (1/N)
    
    for k in range(100):
        if match_count[k] >= 0.5:
            print k + 1 #Our count was shifted back one unit by indexing
            return
    
    print "0.5 probability exceeded 100 individuals"


birthday_match(20)
birthday_match(40)
birthday_match(100)
birthday_match(1000)
birthday_match(10000)


        