import random
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib

class Human:
    
    def __init__(self, N, v, i, st): #N^2 room and avg velocity v
        self.size = N
        self.pos = np.array([random.uniform(0,N) , random.uniform(0,N)]) #Random spot in the NxN room
        self.vel = np.float64(v) #speed of movement
        self.dir = self.rand_dir() #direction of movement
        self.ID = i #simple number identification (will match list indexing)
        self.status = st #Options are "inf"- infected, "well", "vac"- vaccinated 
    
    
    def rand_dir(self): #Gives a random direction with unit length
        temp_x = random.uniform(-1, 1)
        neg_chance = random.randint(1,2) #Determines pos or neg y value
        temp_y = np.sqrt(1.0 - temp_x**2)*((-1.0)**neg_chance)  #Normalized and equal chances for each solution to preserve random/even distribution
        return np.array([temp_x, temp_y]) #When person reaches a wall, they chance direction
    
    
    def move(self):
        if (self.pos[0]== np.float64(0)) or (self.pos[0]== np.float64(self.size)) or (self.pos[1]== np.float64(0)) or (self.pos[1]== np.float64(self.size)):
            self.dir = self.rand_dir() #When person reaches a wall, they chance direction
        self.pos += self.vel*self.dir
        
        for i in range(2): #Enforces boundaries
            if self.pos[i] > self.size:
                self.pos[i] = np.float64(self.size)
            
            if self.pos[i] < 0:
                self.pos[i] = np.float64(0)


class Room:
    
    def __init__(self, M, N, v, inf, vac, ps):#inf- number infected, vac- number vaccinated
        self.pop = M #Number of people in the room
        self.size = N #NxN size room
        self.people = [] #List of people (objects)
        self.location = [] #List of positions with identical indexing  ###<---SHOULD I REMOVE THIS?
        self.vlc = v #velocity
        self.clock = 0 #time counter
        self.normies = [] #list of the indexes/id's of the well/not infected
        self.infected = [] #list of indexes/id's of the infected
        self.vacs = [] #list of the indexes/id's of the vaccinated
        self.p_space = ps #Denotes if personal space is in effect options "Y"-yes, "N"-no 
        
        for i in range(M):
            if i < vac:
                self.people += [Human(N, v, i, "vac")] #Constructs vaccinated people
                self.location += [self.people[i].pos]
                self.vacs += [i]
            
            elif vac <= i < (vac + inf):
                self.people += [Human(N, v, i, "inf")] #Constructs infected people
                self.location += [self.people[i].pos]
                self.infected += [i]
            
            else:
                self.people += [Human(N, v, i, "well")] #Constructs well/not infected/normal people
                self.location += [self.people[i].pos]
                self.normies += [i]
                
    
    
    def update(self): #Updates movement one step
        self.clock += 1
        
        for i in range(self.pop):
            self.people[i].move()
        
        for i in range(len(self.infected)): #Locates all people sufficiently close to each infected
            k = self.infected[i] #The id of the infected
            to_remove = [] #temp list of indices to remove in the normies list
            for j in range(len(self.normies)):
                w = self.normies[j] #The id of the well/not infected
                temp_dist = np.linalg.norm(self.people[k].pos - self.people[w].pos) #distance from normie to infected
                if temp_dist < 3.0*self.vlc: #Caps the range for potential infection at 2xvelocity
                    if uniform_exp() < np.exp(-temp_dist): #Chance roll of getting infected
                        self.people[w].status = "inf"
                        self.infected += [w] #Although this adds to the infected list, the for loop will not reach this id during this iteration/update
                        to_remove += [j]
                        #print "Person", self.people[w].ID, "was infected by", self.people[k].ID, "!" 
                        #print "Total infected:", len(self.infected)
            
            for j in range(len(to_remove)):
                v = to_remove[j] - j #minus j accounts for the list decreasing by 1 after each iteration b/c the to_remove list is strictly increasing
                self.normies = self.normies[:v] + self.normies[v+1:]
        
        if self.p_space == "Y": #Makes people back up if they're too close together
            bndry = self.vlc*1.0 #Takes effect when people are a step away from one another or closer
            for i in range(len(self.people)-1):
                for j in range(len(self.people[i+1:])):
                    temp_v = self.people[i].pos - self.people[i+j+1].pos #vector defining distance and angle between two people
                    if np.linalg.norm(temp_v) <= bndry:
                        temp_d = 0.5*(bndry - np.linalg.norm(temp_v))
                        self.people[i].pos += temp_d*temp_v #both people back up slightly
                        self.people[i+j+1].pos -= temp_d*temp_v
                        self.people[i].dir = self.people[i].rand_dir()
                        self.people[i+j+1].dir = self.people[i+j+1].rand_dir()
        

    
    def update_scene(self, event): #Watch the spread of the infection
        
        if event.key == "q": #press q to quit the progression
            plt.close()
            
        if event.key =="enter": #press enter to move forward a Step
            self.update_scene_helper()
        
        while (event.key =="shift") and (len(self.normies)> 0): #press shift to watch the entire infection
            self.update_scene_helper()
        
            
            
    def update_scene_helper(self): #redraws each new plot    
            plt.clf()
            self.update()
            
            x_inf = []
            y_inf = []
            x_norm = []
            y_norm = []
            x_vac =[]
            y_vac = []
            for i in range(len(self.infected)): #Infected - red x mark
                k = self.infected[i]
                x_inf += [self.people[k].pos[0]]
                y_inf += [self.people[k].pos[1]]
            
            for i in range(len(self.vacs)): #Vaccinated - yellow square
                k = self.vacs[i]
                x_vac += [self.people[k].pos[0]]
                y_vac += [self.people[k].pos[1]]
            
            for i in range(len(self.normies)): #Normal/well - blue circle
                k = self.normies[i]
                x_norm += [self.people[k].pos[0]]
                y_norm += [self.people[k].pos[1]]
                
            
            plt.scatter(x_norm, y_norm, lw = 0)
            plt.scatter(x_inf, y_inf, marker= 'x', c='r')
            plt.scatter(x_vac, y_vac, marker= 's', c='y', lw = 0)
            plt.axis([-5, self.size +5 , -5, self.size + 5])
            fig.canvas.draw()
##########################################################

def uniform_exp(): #On the range from zero to pos infinity and assuming exponential decay
    x = random.uniform(0,1)
    return -1.0*np.log(1 - x)
    
##########################################################    


def plot_scene(room): #Plots the current location of all persons, with health status denoted by point shape
    
    x_inf = []
    y_inf = []
    x_norm = []
    y_norm = []
    x_vac =[]
    y_vac = []
    for i in range(len(room.infected)): #Infected - red x mark
        k = room.infected[i]
        x_inf += [room.people[k].pos[0]]
        y_inf += [room.people[k].pos[1]]
    
    for i in range(len(room.vacs)): #Vaccinated - yellow square
        k = room.vacs[i]
        x_vac += [room.people[k].pos[0]]
        y_vac += [room.people[k].pos[1]]
    
    for i in range(len(room.normies)): #Normal/well - blue circle
        k = room.normies[i]
        x_norm += [room.people[k].pos[0]]
        y_norm += [room.people[k].pos[1]]
    
    plt.scatter(x_norm, y_norm, lw = 0)
    plt.scatter(x_inf, y_inf, marker= 'x', c='r')
    plt.scatter(x_vac, y_vac, marker= 's', c='y', lw = 0)
    plt.axis([-5, room.size + 5, -5, room.size +5])
    plt.show()

    

"""
m = input("Enter number of people:")
n = input("Enter square root of room area:")
v = input("Enter velocity:")
room_run = Room(m, n, v)
print "Initial Positions:"
print room_run.location
room_run.pass_time(1) #NOTE!!! pass_time() has been replaced by update()
print "\nPosition after 1 second:"
print room_run.location
room_run.pass_time(5)
print "\nPosition after additional 5 seconds:"
print room_run.location
room_run.pass_time(10)
print "\nPosition after additional 10 seconds:"
print room_run.location

########################################

room_run = Room(1, 100, 3.3) #Watch one person walk around the room##VVV REMOVE LATER VVV

x_plot =[]
y_plot = []

for i in range(120):
    x_plot += [room_run.people[0].pos[0]]
    y_plot += [room_run.people[0].pos[1]]
    room_run.pass_time(1)


plt.scatter(x_plot, y_plot, c= 'r')
plt.show()"""

##########################################

room_run= Room(100, 100, 3.3, 1, 5, "N")#(Room of people, Room size, speed, num infected, num vaccinated, personal space - "Y/N")

################## Time to Complete Infection ########################


#while len(room_run.normies) > 0:
#    room_run.update()
#print "Total time:", room_run.clock

############ To Watch Progression of Infection ##########################

#Press enter to iterate, shift to run the entire infection, q to quit
print "100x100 room with 94 normal people, 1 infected, and 5 vaccinated"
print "Red X - Infected, Yellow Square- Vaccinated, Blue Dot - Normal"
print "Press enter to iterate, shift to run the entire infection, q to quit"
fig, ax = plt.subplots()
fig.canvas.mpl_connect('key_press_event', room_run.update_scene)
plot_scene(room_run)
print "Time:", room_run.clock

################# Data Gathering #####################

"""

#Newly infected vs time

x_plot = range(0,1000, 20)
y_plot = []
for i in range(len(x_plot)):
    y_plot += [[]]

for i in range(150): #Number of trials
    print i
    room_run = Room(100, 100, 3.3, 1, 0, "Y")
    old_count = len(room_run.infected) #Used to record how many new people have been infected
    while len(room_run.normies) > 0:
        if room_run.clock % 10 == 0:
            j = room_run.clock/10
            y_plot[j] += [len(room_run.infected) - old_count]
            old_count = len(room_run.infected)
        room_run.update()
        
    for i in range(len(x_plot)- len(y_plot)):
        y_plot[-i -1] += [0] 
                
for i in range(len(y_plot)):
    y_plot[i] = np.average(y_plot[i]) #takes average of all trials for each index

while (y_plot[-1] == 0.0) or (math.isnan(y_plot[-1])):
    y_plot = y_plot[:-1]
    x_plot = x_plot[:-1]
    
plt.scatter(x_plot, y_plot, s = 300, c='g')
plt.xlabel("Time", fontsize= 32)
plt.ylabel("New Infected", fontsize = 32)
plt.tick_params(labelsize=20)
plt.title("Number of Additional People Infected vs Time (Personal Space Enabled)", fontsize = 36)
plt.show()


#Time for complete infection vs number vaccinated

x_plot = range(0, 50, 5)
y_plot = []

for elt in x_plot:
    print elt
    trials = []
    
    for i in range(20):
        print "-", i
        room_run = Room(100, 100, 3.3, 1, elt, "Y")
        while len(room_run.normies) > 0: 
            room_run.update()
        trials += [room_run.clock]
    
    y_plot += [np.average(trials)]
    

plt.scatter(x_plot, y_plot, s = 300, c = 'y')
plt.xlabel("Number Vaccinated", fontsize= 32)
plt.ylabel("Time to Complete Infection", fontsize = 32)
plt.tick_params(labelsize=20)
plt.title("Number of People Vaccinated vs Time (Personal Space Enabled)", fontsize = 36)
plt.show()"""
