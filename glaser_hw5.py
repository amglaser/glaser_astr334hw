import numpy as np
import random
import matplotlib.pyplot as plt

def Bisec(f, a, b, n): #find a root of function f() on interval [a,b] #Increase max iteration count by factor of n #By default set n=1
    
    a_org =a
    b_org = b
    
    mid = random.uniform(a,b) #starts with a random float in range [a,b]
    count = 0
    
    while count < n*(10**10):
                
        if abs(f(mid)) < 10**(-5):
            return mid
        if (f(a)/f(mid)) < 0:
            b = mid
        if (f(b)/f(mid)) < 0:
            a = mid
        
        mid = (a+b)/2
        count += 1
    
    return Bisec(f,a_org,b_org) #in case count is maxed out, we restart the function with a new random initial choice for the first bisection
    
def f(x):
    X = np.float64(x)
    return np.sin(X)

def g(x):
    X = np.float64(x)
    return (X**3)-X-2

def y(x):
    X = np.float64(x)
    return -6 + X + (X**2)


print "f root:", Bisec(f, 2, 4, 1)
print "g root:", Bisec(g, 1, 2, 1)
print "y root:", Bisec(y, 0, 5, 1)

print "Plot color code: f - blue, g - red, y - yellow"

x_f = np.linspace(2,4,100)
y_f = f(x_f)

x_g= np.linspace(1,2,50)
y_g = g(x_g)

x_y= np.linspace(0,5,100)
y_y = y(x_y)


plt.scatter(x_y, y_y, c = 'y')
plt.scatter(x_f, y_f, c= 'b')
plt.scatter(x_g, y_g, c= 'r')
plt.show()