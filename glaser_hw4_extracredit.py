import numpy as np
import math as math
import matplotlib.pyplot as plt

def f(x):
    X = np.float64(x)
    return np.float64((X**3)+2.0*(X**2) - 4.0)


def piecewise_int(n):
    N = np.float64(n)
    
    temp_intgl = 0
    for i in range(-1*n,n): #Integration is partitioned into pieces of length 1/N
        temp_intgl += (f((i)/N))/N
    print "Piecewise:", temp_intgl
    print "Error:", abs(temp_intgl + (20.0/3.0))


def trap_int(n):
    N = np.float64(n)
    
    temp_intgl = 0
    for i in range(-1*n,n): #Integration is partitioned into pieces of length 1/N
        temp_intgl += ((f((i+1.0)/N) + f(i/N))/(2.0*N))
    print "Trapezoidal:", temp_intgl
    print "Error:", abs(temp_intgl + (20.0/3.0))


def simp_int(n):
    N = np.float64(n)
    
    temp_intgl = 0
    for i in range(-1*n,n): #Integration is partitioned into pieces of length 1/N
        temp_intgl += ((f((i+1.0)/N) + f(i/N) + 4.0*f((i + 0.5)/N))/(6.0*N))
    print "Simpson's Rule:", temp_intgl
    print "Error:", abs(temp_intgl + (20.0/3.0))


def GQ_int():
    
    temp_intgl = np.float64(f(-1.0/math.sqrt(3.0)) + f(1.0/math.sqrt(3.0)))
    print "Gaussian Quadrature:", temp_intgl
    print "Error:", abs(temp_intgl + (20.0/3.0))


piecewise_int(23000)
print "-----"
trap_int(1200)
print "-----"
simp_int(6)
print "-----"
GQ_int()

